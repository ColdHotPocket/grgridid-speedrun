# grGRIDid

## What?

An entry to the Ludum Dare 48 Compo, with added features

## Who?

JohnGabrielUK made the original for the Ludum Dare Compo. I, ColdCalzone, am simply making the game more bearable to run

## Why?

Because of the built-in timer, this game is incredibly easy to speedrun. I, however, want to make the game better to speedrun.

## Can I?

Probably - the game is licensed under GNU GPL v3. The art and music are [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
