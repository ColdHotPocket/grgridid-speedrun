extends KinematicBody

onready var sprite : Sprite3D = $Sprite3D
onready var area_jumppoint_me : Area = $Area_JumpPointMe
onready var area_jumppoint_smaller_u : Area = $Area_JumpPointSmallerU
onready var area_jumppoint_larger_u : Area = $Area_JumpPointLargerU
onready var area_jumppoint_smaller_d : Area = $Area_JumpPointSmallerD
onready var area_jumppoint_larger_d : Area = $Area_JumpPointLargerD
onready var area_jumppoint_smaller_l : Area = $Area_JumpPointSmallerL
onready var area_jumppoint_larger_l : Area = $Area_JumpPointLargerL
onready var area_jumppoint_smaller_r : Area = $Area_JumpPointSmallerR
onready var area_jumppoint_larger_r : Area = $Area_JumpPointLargerR
onready var tween : Tween = $Tween

const MOVE_SPEED : float = 4.0
const ANIM_SPEED : float = 4.0
const ASCENDING_SPEED : float = 2.0
const ASCENDING_SPIN_SPEED : float = 10.0

const BGM_SCALE : Array = [1.0, 0.5, 0.25]

enum PlayerState {NORMAL, MOVING, JUMPING, ASCENDING}

var movement_remaining : Vector3 = Vector3.ZERO
var facing_direction : Vector3 = Vector3.RIGHT
var anim_index : float = 0.0

var current_state : int = PlayerState.NORMAL
var current_scale : int = 0

signal jumping_in
signal jumping_out
signal goal_reached

func turn_to_direction(angle : float) -> void:
	# Make sure we aren't about to turn more than 180 degrees (it'd look silly)
	var angle_diff : float = abs(sprite.rotation_degrees.y - angle)
	if angle_diff > 180.0:
		if sprite.rotation_degrees.y > angle:
			sprite.rotation_degrees.y -= 360.0
		else:
			sprite.rotation_degrees.y += 360.0
	# Sprite go spin
	tween.interpolate_property(sprite, "rotation_degrees", null, Vector3(0.0, angle, 0.0), 0.125)
	tween.start()

func jump_into(direction : Vector3) -> void:
	AudioController.play_sound("jump_in")
	current_scale += 1
	AudioController.change_bgm_scale(BGM_SCALE[current_scale])
	emit_signal("jumping_in")
	current_state = PlayerState.JUMPING
	sprite.frame = 1
	tween.interpolate_property(self, "translation", null, translation + (direction * 0.6 * scale), 1.0)
	tween.interpolate_property(self, "scale", null, scale / 5.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.interpolate_property(sprite, "translation", Vector3(0.0, 0.64, 0.0), Vector3(0.0, 1.64, 0.0), 0.5, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.interpolate_property(sprite, "translation", Vector3(0.0, 1.64, 0.0), Vector3(0.0, 0.64, 0.0), 0.5, Tween.TRANS_SINE, Tween.EASE_IN, 0.5)
	tween.start()
	yield(tween, "tween_all_completed")
	current_state = PlayerState.NORMAL
	sprite.frame = 0

func jump_out(direction : Vector3) -> void:
	AudioController.play_sound("jump_out")
	current_scale -= 1
	AudioController.change_bgm_scale(BGM_SCALE[current_scale])
	emit_signal("jumping_out")
	current_state = PlayerState.JUMPING
	sprite.frame = 1
	tween.interpolate_property(self, "translation", null, translation + (direction * 3.0 * scale), 1.0)
	tween.interpolate_property(self, "scale", null, scale * 5.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.interpolate_property(sprite, "translation", Vector3(0.0, 0.64, 0.0), Vector3(0.0, 1.64, 0.0), 0.5, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.interpolate_property(sprite, "translation", Vector3(0.0, 1.64, 0.0), Vector3(0.0, 0.64, 0.0), 0.5, Tween.TRANS_SINE, Tween.EASE_IN, 0.5)
	tween.start()
	yield(tween, "tween_all_completed")
	current_state = PlayerState.NORMAL
	sprite.frame = 0

func check_for_smaller_jumppoint(area : Area) -> bool:
	for overlapping_area in area.get_overlapping_areas():
		if overlapping_area.is_in_group("jump_point") and overlapping_area.which_scale == current_scale + 1:
			return true
	return false

func check_for_larger_jumppoint(area : Area) -> bool:
	for overlapping_area in area.get_overlapping_areas():
		if overlapping_area.is_in_group("jump_point") and overlapping_area.which_scale == current_scale - 1:
			return true
	return false

func check_for_same_jumppoint(area : Area) -> bool:
	for overlapping_area in area.get_overlapping_areas():
		if overlapping_area.is_in_group("jump_point") and overlapping_area.which_scale == current_scale:
			return true
	return false

func check_for_goal(area : Area) -> bool:
	for overlapping_area in area.get_overlapping_areas():
		if overlapping_area.is_in_group("goal"):
			return true
	return false

func check_for_smaller_grid(area : Area) -> Area:
	for overlapping_area in area.get_overlapping_areas():
		if overlapping_area.is_in_group("inner_grid") and overlapping_area.which_scale == current_scale + 1:
			return overlapping_area
	return null

func can_jump_into() -> bool:
	match facing_direction:
		Vector3.FORWARD:
			return check_for_smaller_jumppoint(area_jumppoint_smaller_u)
		Vector3.BACK:
			return check_for_smaller_jumppoint(area_jumppoint_smaller_d)
		Vector3.LEFT:
			return check_for_smaller_jumppoint(area_jumppoint_smaller_l)
		Vector3.RIGHT:
			return check_for_smaller_jumppoint(area_jumppoint_smaller_r)
		# We shouldn't ever get here
		_:
			return false

func can_jump_out() -> bool:
	match facing_direction:
		Vector3.FORWARD:
			return check_for_larger_jumppoint(area_jumppoint_larger_u)
		Vector3.BACK:
			return check_for_larger_jumppoint(area_jumppoint_larger_d)
		Vector3.LEFT:
			return check_for_larger_jumppoint(area_jumppoint_larger_l)
		Vector3.RIGHT:
			return check_for_larger_jumppoint(area_jumppoint_larger_r)
		# We shouldn't ever get here
		_:
			return false

func try_to_jump() -> bool:
	# Are we standing in a jump-point?
	if check_for_same_jumppoint(area_jumppoint_me):
		if can_jump_into():
			jump_into(facing_direction)
			return true
		elif can_jump_out():
			jump_out(facing_direction)
			return true
	return false

func try_to_move(direction : Vector3) -> void:
	facing_direction = direction
	# Can we jump in this direction?
	var jumped : bool = try_to_jump()
	if jumped:
		return
	# Okay, no jumping here - let's try to move normally
	if test_move(transform, direction * scale):
		return
	movement_remaining = direction * scale
	current_state = PlayerState.MOVING
	anim_index = 1.0

func try_to_spin() -> void:
	# We can't spin a grid if there isn't a connecting point, so check for that first
	if not can_jump_into():
		return
	var inner_grid : Area = null
	match facing_direction:
		Vector3.FORWARD:
			inner_grid = check_for_smaller_grid(area_jumppoint_smaller_u)
		Vector3.BACK:
			inner_grid = check_for_smaller_grid(area_jumppoint_smaller_d)
		Vector3.LEFT:
			inner_grid = check_for_smaller_grid(area_jumppoint_smaller_l)
		Vector3.RIGHT:
			inner_grid = check_for_smaller_grid(area_jumppoint_smaller_r)
	if inner_grid != null:
		inner_grid.spin_clockwise()

func state_normal(delta : float) -> void:
	if Input.is_action_just_pressed("move_up"):
		turn_to_direction(90.0)
		try_to_move(Vector3.FORWARD)
	elif Input.is_action_just_pressed("move_down"):
		turn_to_direction(-90.0)
		try_to_move(Vector3.BACK)
	elif Input.is_action_just_pressed("move_left"):
		turn_to_direction(180.0)
		try_to_move(Vector3.LEFT)
	elif Input.is_action_just_pressed("move_right"):
		turn_to_direction(0.0)
		try_to_move(Vector3.RIGHT)
	elif Input.is_action_just_pressed("spin"):
		try_to_spin()
	if check_for_goal(area_jumppoint_me):
		current_state = PlayerState.ASCENDING
		emit_signal("goal_reached")

func state_moving(delta : float) -> void:
	var amount_to_move : float = MOVE_SPEED * delta * scale.x
	# Make sure we aren't about to overshoot
	if movement_remaining.length() < amount_to_move:
		movement_remaining = Vector3.ZERO
		translation = translation.snapped(scale)
		current_state = PlayerState.NORMAL
		sprite.frame = 0
		anim_index = 0
	else:
		if movement_remaining.x > 0.0:
			movement_remaining.x -= amount_to_move
			translation.x += amount_to_move
		if movement_remaining.x < 0.0:
			movement_remaining.x += amount_to_move
			translation.x -= amount_to_move
		if movement_remaining.z > 0.0:
			movement_remaining.z -= amount_to_move
			translation.z += amount_to_move
		if movement_remaining.z < 0.0:
			movement_remaining.z += amount_to_move
			translation.z -= amount_to_move
		anim_index += ANIM_SPEED * delta
		sprite.frame = wrapf(anim_index, 0.0, 2.0)

func state_ascending(delta : float) -> void:
	translation.y += ASCENDING_SPEED * scale.y * delta
	sprite.translation.y += ASCENDING_SPEED * scale.y * 0.5 * delta
	rotation.y += ASCENDING_SPIN_SPEED * delta

func _physics_process(delta : float) -> void:
	match current_state:
		PlayerState.NORMAL:
			state_normal(delta)
		PlayerState.MOVING:
			state_moving(delta)
		PlayerState.ASCENDING:
			state_ascending(delta)
	if Input.is_action_just_pressed("restart_game"):
		get_tree().change_scene("res://scenes/TitleScreen.tscn")
	elif Input.is_action_just_pressed("restart_level"):
		get_tree().reload_current_scene()
