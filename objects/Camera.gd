extends Camera

onready var tween : Tween = $Tween
onready var fps : Label = $Label

export (NodePath) var path_player

onready var player : Spatial = get_node(path_player)

func _process(delta : float) -> void:
	var target_transform = transform.looking_at(player.translation, Vector3.UP)
	transform = transform.interpolate_with(target_transform, delta * 5.0)
	fps.text = "FPS: " + String(Engine.get_frames_per_second())

func zoom_in() -> void:
	tween.interpolate_property(self, "size", null, size / 5.0, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()

func zoom_out() -> void:
	tween.interpolate_property(self, "size", null, size * 5.0, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()

func _ready() -> void:
	AudioController.change_bgm_scale(1.0)
	player.connect("jumping_in", self, "zoom_in")
	player.connect("jumping_out", self, "zoom_out")
	# Fancy intro
	tween.interpolate_property(self, "v_offset", null, 0.0, 1.0, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.start()

func _enter_tree() -> void:
	v_offset = 10.0
