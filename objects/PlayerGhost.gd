extends Sprite3D

onready var tween : Tween = $Tween

func _on_Tween_tween_all_completed() -> void:
	queue_free()

func _process(delta : float) -> void:
	var angleish : float = wrapf(stepify(rotation_degrees.y, 90.0), 0.0, 180.0)
	if angleish == 90.0:
		translation.x -= 0.01 * delta
	else:
		translation.z -= 0.01 * delta

func _ready() -> void:
	tween.interpolate_property(self, "modulate", null, Color.transparent, 0.25)
	tween.start()
