extends Spatial

export (int) var which_scale
export (bool) var can_rotate

onready var tween : Tween = $Tween

func spin_clockwise() -> void:
	if can_rotate and not tween.is_active():
		tween.interpolate_property(self, "rotation_degrees", null, rotation_degrees + Vector3(0.0, 90.0, 0.0), 0.5, Tween.TRANS_SINE, Tween.EASE_OUT)
		tween.start()
