extends Control

onready var label : Label = $Label
onready var tween : Tween = $Tween

func _ready() -> void:
	var clear_time : float = round((Levels.end_time - Levels.start_time))
	var clear_time_minutes : int = int(floor(clear_time / 60000.0))
	var clear_time_seconds : int = int(clear_time / 1000.0) % 60
	var clear_time_milli : int = int(clear_time) % 1000
	label.text = "Your time was %d:%s.%d.\r\nThank you for playing!" % [clear_time_minutes, "0" + String(clear_time_seconds) if len(String(clear_time_seconds)) == 1 else String(clear_time_seconds), clear_time_milli]
	tween.interpolate_property(label, "modulate", Color.transparent, Color.white, 2.0, Tween.TRANS_LINEAR, Tween.EASE_OUT, 2.0)
	tween.start()
	yield(tween, "tween_all_completed")

func _input(event : InputEvent):
	if event.is_action_pressed("ui_accept"):
		get_tree().change_scene("res://scenes/TitleScreen.tscn")

func _enter_tree() -> void:
	$Label.modulate = Color.transparent
