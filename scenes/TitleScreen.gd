extends Spatial

onready var menu : Control = $CanvasLayer/Menu
onready var fadein : ColorRect = $CanvasLayer/Fadein
onready var camera : Camera = $CameraArm/Camera
onready var tween : Tween = $Tween

func _input(event : InputEvent) -> void:
	if event.is_action_pressed("start"):
		tween.interpolate_property(menu, "rect_position", null, Vector2(0.0, 720), 1.0, Tween.TRANS_SINE, Tween.EASE_IN)
		tween.interpolate_property(camera, "v_offset", null, 3.0, 1.0, Tween.TRANS_SINE, Tween.EASE_IN)
		tween.start()
		yield(tween, "tween_all_completed")
		Levels.new_game()

func _ready() -> void:
	yield(get_tree().create_timer(0.25), "timeout")
	tween.interpolate_property(fadein, "modulate", null, Color.transparent, 1.0)
	tween.start()

func _enter_tree() -> void:
	$CanvasLayer/Fadein.modulate = Color.white
